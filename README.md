# GoAPI

A Go API package to take your projects' structs and render them as JSON payloads.
This package attempts to follow the JSON API spec [http://jsonapi.org](http://jsonapi.org), be generic enough so easy to use and strict enough to enforce some good behaviours.
This package also implements the Go stdLib `"encoding/json"` package so `MarshalJSON()` &amp; `UnmarshalJSON(b []byte)` calls are respected.

If you like this package or are using for your own needs, then let me know via [https://twitter.com/kylehqcom](https://twitter.com/kylehqcom)

To include this package first `go get -u gitlab.com/kylehqcom/gojsonapi` and then `import "gitlab.com/kylehqcom/gojsonapi"`

## Usage - Writing Out

To write your HTTP JSON response, create a new response object.

```go
package main

import "gitlab.com/kylehqcom/gojsonapi"

type Foo struct {
	Name string `json:"name"`
}

func Handler(w http.ResponseWriter, r *http.Request) {
	res := goapi.NewResponse()

	f := &Foo{}
	f.Name = "Bar"
	res.Add(f)

    res.WriteHTTPResponse(w)
}
```

Follow the normal json requirements on your struct fields eg `omitempty` to manipulate rendering.
The marshalled json payload from the above would set a 200 HTTP status code (OK) and an array payload of

```json
{ "data": [{ "name": "Bar" }] }
```

### Single Entry vs Array

The response is either a single object at the "data" root or a array based on how you call/bind your data to the response.

If returning a single entity from your resource, call `Set(e interface{})` on your response instance.
This will ensure that the entity is returned as an object, not nested inside an array as per the JSON Api spec.
The above output with `res.Set(f)` would be

```json
{ "data": { "name": "Bar" } }
```

**_Note:_** if you have more than one entry on your response and `Set(e interface{})` is the last call, only the first entry will be returned, all others ignored. Calling `Add(e ...interface{})` again will revert the response back to an array payload.

### Errors

Create a new error with `goapi.NewError()` to return an error struct with the fields of `Title` and `Detail`.
`AddError` binds the error to a slice on the Response so you are free to add as many errors as required.
All errors will be rendered on marshal.

For convenience, if you have an existing Go error, you can instead call `(r *Response) AddError(err error, opts ...APIErrorOption)` directly on the response.
The `AddError` func which will cast the error for you to a `*APIError` and bind against the response.

**Please note**, if any error is assigned to a response, on render no data will be returned as per the JSON API spec. Instead an error response will always be returned.
If a HTTP status code is not set, a 400 will be assigned.
You can check for errors with the `HasErrors()` method and call `ClearErrors()` as required.

### StatusCode

Set the status code at anytime on your response instance with `res.SetStatusCode()`.
By default a 200 OK response is returned if omitted, unless the response instance has errors.

## Usage - Reading In

You can easily read a http.Body &amp; bind back against your Go structs with the `UnmarshalPayload` func.

```go
package main

import "gitlab.com/kylehqcom/gojsonapi"

type Foo struct {
	Name string `json:"name"`
}

func Handler(w http.ResponseWriter, r *http.Request) {
	res := goapi.NewResponse()
	foo := &Foo{}
	err := res.UnmarshalPayload(r.Body, foo)
	if err != nil {
		res.AddError(err)
		res.WriteHTTPResponse(w)
		return
	}

	if foo.Name == "Fred" {
		...
	}
}
```
